# AK Lokalpolitik

Hier wird die Webseite des AK Lokalpolitik Augsburg gebaut. Wir bitten um etwas Geduld.

## Ich will mitmachen!

Super! Jeder kann bei uns mitmachen. Um Gesprächsthemen vor zu schlagen, oder bei gesprächen mit zu machen schaue bei uns in der [Telegram Gruppe](https://t.me/localpolitics_aux) vorbei.

## Protkolle

Protokolle über vergangene Gespräche finden Sie [hier](/protokolle).

## Themen

Eine übersicht über alle themen finden Sie [hier](/themen).

## Emails

Weil wir für Transparenz sind, versuchen wir unseren gesamten E-Mail-Verkehr offen zu legen. Momentan arbeiten wir noch an der technischen und rechtlichen Umsetzung.
