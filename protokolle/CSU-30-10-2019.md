---
date: 30.10.2019
title: Protokoll vom Gespräch mit Andreas Jäckel
attendants: Milan, Taulexa, Andreas Jäckel, Anton
hidden: true
---

# Protokoll vom Gespräch mit Andreas Jäckel (CSU) vom 30.1.2019

## Brief von Augsburg Handelt / FFF

* Wird vom Bau- und Wirtschaftsreferat abgearbeitet
* Wird JETZT angeschaut, und nicht auf die lange bank geschoben

## CO2 Steuer / Emissionshandel

* Wir sind gegen Emissionshandel, für uns ist eine CO2-Steuer der bessere Ansatz.
* Ansatz der Umvertelung. Für jedes Gramm CO2 wird ein Betrag X gezahlt. Dieser Betrag wird an alle gleich ausgeschüttet.
  * Das führt zu einem individuellen Drang, CO2 zu reduzieren
  * **Frage:** Ist dieser Ansatz sozial gerecht?
    * Person die in München ein Haus geerbt hat vs. Familienvater der 2h nach München mit dem Auto pendelt
    * => Wir benötigen den Ausbau von ÖP(N)V (Regionalverkehr)
    * => Eventuell ist Pendeln einfach nicht der richtige Ansatz, und sollte reduziert werden.
    * => Essen (und andere Produkte) machen wahrscheinlich einen wesentlich größeren Teil der Steuer aus *RESEARCH REQUIRED*
    * **RESEARCH AQUIRED:** 1Kg Rind = 27Kg CO2, 1L Benzin = 2,3Kg CO2. Käse 9Kg CO2 / Kg. Milch 1Kg CO2 / Kg [source](https://www.oatly.com/nl/climate-footprint)


## Transparentes ausweisen von CO2-Verbrauch / Zucker und anderen Schadstoffen auf allen Lebensmitteln

* Portugal(???) hat bereits diese Markierungen an Lebensmitteln *RESEARCH REQUIRED*
* Oatly hat eine [Kampagne](https://www.oatly.com/de/petition) für CO2-Ausweise auf Lebensmitteln

## 10h Regel

* Im Umkreis von 10\*Höhe des Gebäudes darf kein Windrad stehen oder überholt werden (Bayern only)
* Das ist uncool, vor allem wenn ein Gebäude nachträglich zu einem Windrad gestellt wird
* Das einzig plausible Hauptargument scheint zu sein, dass es schlecht für die Umwelt / Menschen in der Nähe ist (Vögel, Insekten)
  * Kohle, Fensterscheiben, Autos sind wesentlich schlimmer *RESEARCH REQUIRED*
* Nach dem das eine Bayern-only Regel ist und Bayern keine Kohle (den Rohstoff) hat, übergeben wir Verantwortung für Energieerzeugung an andere Bundesländer

## *(unconfirmed)* Support für erneuerbare Energien wurden verringert nachdem Windkraft anfing Kohle zu verdrängen
*RESEARCH REQUIRED!!!*

## Die Innenstadt sollte fahrradfreundlicher werden

* In die STVO aufnehmen, dass in der Innenstadt in 30er Zonen Linienverkehr vor Fußgängern und Fahrradfahrern vor Autos gilt.
* Verringert Autoanteil in der Innenstadt ohne wirkliche Verbote einzuführen
